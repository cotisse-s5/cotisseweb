drop view users_view;
drop view destn_view;
drop view destncateg_view;
drop view destndets_view;


create view users_view as
select concat(users.idusers,profiles.idprofiles) as id, users.*, profiles.label, profiles.weight from users
left join profiles on profiles.idprofiles = users.idprofiles;

CREATE VIEW destn_view AS
SELECT concat(destn.iddestn,categ.idcateg) as id, destn.*, departure.label as departure, arrival.label as arrival, categ.idcateg, categ.label, destncateg.price FROM destn
LEFT JOIN city as departure ON departure.idcity = destn.iddeparture
LEFT JOIN city as arrival ON arrival.idcity = destn.idarrival
LEFT JOIN destncateg ON destncateg.iddestn = destn.iddestn
LEFT JOIN categ ON categ.idcateg = destncateg.idcateg;

create view vehicle_view AS
select concat(vehicle.idvehicle, categ.idcateg) as id, vehicle.*, users.frstName, users.lastName, categ.label from vehicle
LEFT JOIN users on users.idusers = vehicle.idusers
LEFT JOIN categ on categ.idcateg = vehicle.idcateg;

CREATE VIEW destncateg_view AS
SELECT destncateg.iddestncateg, destncateg.iddestn, destncateg.price, destn.iddeparture, destn.idarrival, categ.* FROM destncateg
LEFT JOIN destn ON destn.iddestn = destncateg.iddestn
LEFT JOIN categ ON categ.idcateg = destncateg.idcateg;

create view destndets_view as
select concat(destndets.iddestndets, destndets.idvehicle) as id, destndets.*, vehicle.regNb, categ.idCateg, categ.label, categ.nbcolumn, categ.nbrow,
	destn_view.idDeparture, destn_view.idArrival, destn_view.departure, destn_view.arrival
from destndets
left join vehicle on destndets.idVehicle = vehicle.idVehicle
left join categ on vehicle.idCateg = categ.idCateg
left join destn_view on destndets.idDestn = destn_view.idDestn AND categ.idCateg = destn_view.idCateg;





