-- -----------------------------------------------------
-- Drop view
-- -----------------------------------------------------
DROP VIEW IF EXISTS destncateg_view ;
DROP VIEW IF EXISTS vehicle_view ;
DROP VIEW IF EXISTS destn_view ;
DROP VIEW IF EXISTS users_view;
--DROP VIEW IF EXISTS destncateg_view ;
--DROP VIEW IF EXISTS destn_view ;
--DROP VIEW IF EXISTS users_view;
-- -----------------------------------------------------
-- Drop tabe
-- -----------------------------------------------------
DROP TABLE IF EXISTS billdets ;
DROP TABLE IF EXISTS bill ;
DROP TABLE IF EXISTS reservopts ;
DROP TABLE IF EXISTS options ;
DROP TABLE IF EXISTS reservdets ;
DROP TABLE IF EXISTS chair ;
DROP TABLE IF EXISTS destncateg ;
DROP TABLE IF EXISTS reserv ;
DROP TABLE IF EXISTS destndets ;
DROP TABLE IF EXISTS vehicle ;
DROP TABLE IF EXISTS categ ;
DROP TABLE IF EXISTS users ;
DROP TABLE IF EXISTS brand ;
DROP TABLE IF EXISTS destn ;
DROP TABLE IF EXISTS city ;
DROP TABLE IF EXISTS users ;
DROP TABLE IF EXISTS profiles ;

-- -----------------------------------------------------
-- Table profiles
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS profiles (
  idprofiles VARCHAR(45) NOT NULL PRIMARY KEY,
  label VARCHAR(45) NULL,
  weight INT NULL
);

-- -----------------------------------------------------
-- Table users
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS users (
  idusers VARCHAR(45) NOT NULL PRIMARY KEY,
  idprofiles VARCHAR(45) NOT NULL REFERENCES profiles (idprofiles),
  frstname VARCHAR(45) NULL,
  lastname VARCHAR(45) NULL,
  birthday VARCHAR(45) NULL,
  email VARCHAR(45) NULL,
  num VARCHAR(45) NULL,
  pwd VARCHAR(100) NULL,
  token VARCHAR(100) NULL,
  expdate TIMESTAMP NULL,
  state INT NULL
);

-- -----------------------------------------------------
-- Table city
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS city (
  idcity VARCHAR(45) NOT NULL PRIMARY KEY,
  label VARCHAR(45) NULL
);


-- -----------------------------------------------------
-- Table destn
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS destn (
  iddestn VARCHAR(45) NOT NULL PRIMARY KEY,
  iddeparture VARCHAR(45) NOT NULL  REFERENCES city (idcity),
  idarrival VARCHAR(45) NOT NULL  REFERENCES city (idcity)
);


-- -----------------------------------------------------
-- Table brand
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS brand (
  idbrand VARCHAR(45) NOT NULL PRIMARY KEY,
  label VARCHAR(45) NULL
);


-- -----------------------------------------------------
-- Table categ
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS categ (
  idcateg VARCHAR(45) NOT NULL PRIMARY KEY,
  label VARCHAR(45) NULL,
  nbColumn INT NULL,
  nbChair INT NULL,
  points VARCHAR(500) NULL,
  description VARCHAR(500) NULL
);


-- -----------------------------------------------------
-- Table vehicle
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS vehicle (
  idvehicle VARCHAR(45) NOT NULL PRIMARY KEY,
  idbrand VARCHAR(45) NOT NULL  REFERENCES brand (idbrand),
  idusers VARCHAR(45) NOT NULL  REFERENCES users (idusers),
  idcateg VARCHAR(45) NOT NULL  REFERENCES categ (idcateg),
  regnb VARCHAR(45) NULL,
  nbColumn INT NULL,
  nbchair INT NULL,
  state INT NULL
);


-- -----------------------------------------------------
-- Table destndets
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS destndets (
  iddestndets VARCHAR(45) NOT NULL PRIMARY KEY,
  iddestn VARCHAR(45) NOT NULL REFERENCES destn (iddestn),
  idvehicle VARCHAR(45) NOT NULL REFERENCES vehicle (idvehicle),
  departureDate TIMESTAMP NULL,
  arrivalDate TIMESTAMP NULL,
  destnPrice DECIMAL(15,2),
  state INT NULL
);


-- -----------------------------------------------------
-- Table reserv
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS reserv (
  idreserv VARCHAR(45) NOT NULL PRIMARY KEY,
  idusers VARCHAR(45) NOT NULL REFERENCES users (idusers),
  iddestndets VARCHAR(45) NOT NULL REFERENCES destndets (iddestndets),
  reservDate TIMESTAMP NULL
);


-- -----------------------------------------------------
-- Table destncateg
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS destncateg (
  iddestncateg VARCHAR(45) NOT NULL PRIMARY KEY,
  iddestn VARCHAR(45) NOT NULL  REFERENCES destn (iddestn),
  idcateg VARCHAR(45) NOT NULL  REFERENCES categ (idcateg),
  price DECIMAL(15,2) NULL
);


-- -----------------------------------------------------
-- Table chair
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS chair (
  idchair VARCHAR(45) NOT NULL PRIMARY KEY,
  idvehicle VARCHAR(45) NOT NULL  REFERENCES vehicle (idvehicle),
  num int NULL,
  state int NULL
);

-- -----------------------------------------------------
-- Table reservdets
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS reservdets (
  idreservdets VARCHAR(45) NOT NULL PRIMARY KEY,
  idreserv VARCHAR(45) NOT NULL REFERENCES reserv (idreserv),
  idchair VARCHAR(45) NOT NULL REFERENCES chair (idchair)
);


-- -----------------------------------------------------
-- Table options
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS options (
  idoptions VARCHAR(45) NOT NULL PRIMARY KEY,
  label VARCHAR(45) NULL,
  nb INT NULL,
  up DECIMAL(15,2) NULL
);


-- -----------------------------------------------------
-- Table reservopts
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS reservopts (
  idreservopts VARCHAR(45) NOT NULL PRIMARY KEY,
  idreserv VARCHAR(45) NOT NULL REFERENCES reserv (idreserv),
  idoptions VARCHAR(45) NOT NULL REFERENCES options (idoptions)
);


-- -----------------------------------------------------
-- Table bill
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS bill (
  idbill VARCHAR(45) NOT NULL PRIMARY KEY,
  idreserv VARCHAR(45) NOT NULL  REFERENCES reserv (idreserv),
  totalamount DECIMAL(15,2) NULL
);


-- -----------------------------------------------------
-- Table billdets
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS billdets (
  idbilldets VARCHAR(45) NOT NULL PRIMARY KEY,
  idbill VARCHAR(45) NOT NULL REFERENCES bill (idbill),
  label VARCHAR(45) NULL,
  amount DECIMAL(15,2) NULL
);

