package com.cotisse.tools;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class Sha1 {
	public static String encipher(String toEncrypt) throws Exception{
		try {
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
	        crypt.reset();
	        crypt.update(toEncrypt.getBytes("UTF-8"));
	        toEncrypt = byteToHex(crypt.digest());
			return toEncrypt;
		}
	    catch(NoSuchAlgorithmException e){
	        e.printStackTrace();
	        throw new Exception("Algorithm used to encrypt string doesn't exist");
	    }catch(UnsupportedEncodingException e){
	        e.printStackTrace();
	        throw new Exception("Algorithm used to encrypt string is not supported");
	    }
	}
	
	public static boolean checkSHA1(String txt, String txtEncrypted) throws Exception {
		txt = Sha1.encipher(txt);
		return txt.equals(txtEncrypted);
	}
	
	private static String byteToHex(final byte[] hash){
	    Formatter formatter = new Formatter();
	    for (byte b : hash)
	        formatter.format("%02x", b);
	    String result = formatter.toString();
	    formatter.close();
	    return result;
	}
}
