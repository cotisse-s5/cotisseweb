package com.cotisse.tools;

import java.util.Map;

public class Req {
	public static Map<String, Object> data;

	public Req() {}
	public Req(Map<String, Object> req) {
		Req.data = req;
	}

	public static <T> T get(String key) {
		return (T)Req.data.get(key);
	}
}
