package com.cotisse.tools;

import java.sql.Timestamp;

public class Tstp {
	public static Timestamp addDay(Timestamp tstp, int day) {
		return new Timestamp(tstp.getTime() + day * 24 * 60 * 60 * 1000);
	}
	
	public static Timestamp addHr(Timestamp tstp, int hr) {
		return new Timestamp(tstp.getTime() + hr * 60 * 60 * 1000);
	}
	
	public static Timestamp addMin(Timestamp tstp, int min) {
		return new Timestamp(tstp.getTime() + min * 60 * 1000);
	}
	
	public static Timestamp addSec(Timestamp tstp, int sec) {
		return new Timestamp(tstp.getTime() + sec * 1000);
	}
	
	public static Timestamp addTime(Timestamp tstp, int hr, int min, int sec) {
		return new Timestamp(tstp.getTime() + ((hr * 60 + min) * 60 + sec ) * 1000);
	}
}
