package com.cotisse.tools;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Init{
	public static <T> void set(T object, Map<String, Object> req) throws Exception {
		Field[] fields = Init.getFields(object.getClass());
		Method[] meths = Init.getMethods(object.getClass(), fields);
		for(int i = 0; i < fields.length; i++) {
			for(Map.Entry<String, Object> entry : req.entrySet()) {
				if(fields[i].getName().toLowerCase().equals(entry.getKey().toLowerCase())) {
					switch (fields[i].getType().getSimpleName().toLowerCase()) {
						case "int":
							meths[i].invoke(object, Integer.parseInt(entry.getValue().toString()));
							break;
						case "double":
							meths[i].invoke(object, Double.parseDouble(entry.getValue().toString()));
							break;
						case "timestamp":
							try {
								DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
								Date date = formatter.parse(entry.getValue().toString());
								Timestamp tstpDate = new Timestamp(date.getTime());
								meths[i].invoke(object, tstpDate);
							}catch(ParseException e) {
								DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
								Date date = formatter.parse(entry.getValue().toString());
								Timestamp tstpDate = new Timestamp(date.getTime());
								meths[i].invoke(object, tstpDate);
							}
							break;
						default:
							meths[i].invoke(object, entry.getValue().toString());
							break;
					}
				}
			}
		}
	}
	
	public static Method[] getMethods(Class<?> c, Field[] fields) throws Exception {
		int nbFields = fields.length;
		Method[] methods = new Method[nbFields];
		for(int i = 0; i < nbFields; i++)
			methods[i] = c.getMethod("set" + toUpperCaseFirstLetter(fields[i].getName()), fields[i].getType());
		return methods;
	}
	
	public static <T> Field[] getFields(Class<?> c) {
		if(c.getSuperclass() != null) {
			List<Field> fields = new ArrayList<Field>();
			for(Field tmp:c.getDeclaredFields())
				fields.add(tmp);
			for(Field tmp:getFields(c.getSuperclass()))
				fields.add(tmp);
			return fields.toArray(new Field[fields.size()]);
		}
		return c.getDeclaredFields();
	}
	
	public static String toUpperCaseFirstLetter(String arg) {
		char[] chars = arg.toCharArray();
		chars[0] = Character.toUpperCase(chars[0]);
		return String.valueOf(chars);
	}
}
