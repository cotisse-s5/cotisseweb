package com.cotisse.tools;

import java.util.HashMap;

public class RespMsg extends HashMap<String, String>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RespMsg() {}
	
	public RespMsg(String key, String value) {
		this.put(key, value);
	}
}
