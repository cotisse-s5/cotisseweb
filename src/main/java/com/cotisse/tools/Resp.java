package com.cotisse.tools;

public class Resp {
	int status;
	Object data = null;
	Object msg;
	public static final int ERR = 401;
	public static final int SUCCESS = 201;
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getMsg() {
		return msg;
	}

	public void setMsg(Object msg) {
		this.msg = msg;
	}

	public static int getErr() {
		return ERR;
	}

	public static int getSuccess() {
		return SUCCESS;
	}
	
	public Resp() {}
	
	public Resp(int status, Object data, Object msg) {
		this.status = status;
		this.data = data;
		this.msg = msg;
	}
	
	public Resp(int status, Object msg) {
		this.status = status;
		this.msg = msg;
	}
}
