package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.VehicleView;

@Repository
public interface VehicleViewRepo extends JpaRepository<VehicleView, Long>{
	VehicleView[] findByIdCateg(String idCateg);
}
