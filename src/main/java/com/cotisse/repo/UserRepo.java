package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long>{
	User findByEmail(String email);
	User findByNum(String num);
}
