package com.cotisse.repo;

import java.sql.Timestamp;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cotisse.model.DestnDetsView;

public interface DestnDetsViewRepo extends JpaRepository<DestnDetsView, Long> {
	@Query(value="SELECT * FROM destndets_view d WHERE d.iddeparture like ? AND d.idarrival like ? AND d.departuredate\\:\\:date = ?", nativeQuery = true)
	DestnDetsView[] findByIdDepartureAndIdArrivalAndDepartureDate(String idDeparture, String idArrival, Timestamp departure);
}
