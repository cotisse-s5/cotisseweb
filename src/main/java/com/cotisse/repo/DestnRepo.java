package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.Destn;

@Repository
public interface DestnRepo extends JpaRepository<Destn, Long>{

	Destn findByIdDepartureAndIdArrival(String idDeparture, String idArrival);
	Destn findByIdDestn(String idDestn);
}
