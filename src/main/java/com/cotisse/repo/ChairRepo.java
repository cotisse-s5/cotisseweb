package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.Chair;

@Repository
public interface ChairRepo extends JpaRepository<Chair, Long>{

}
