package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.Vehicle;

@Repository
public interface VehicleRepo extends JpaRepository<Vehicle, Long>{

	Vehicle findByRegNb(String regNb);
	Vehicle findByIdVehicle(String vehicle);
	Vehicle findByIdVehicleAndState(String vehicle, int state);
}
