package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.Categ;

@Repository
public interface CategRepo extends JpaRepository<Categ, Long>{
	Categ findByIdCateg(String idCateg);
}
