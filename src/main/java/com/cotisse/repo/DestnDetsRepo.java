package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.DestnDets;

@Repository
public interface DestnDetsRepo extends JpaRepository<DestnDets, Long>{
	
	DestnDets findTop1ByIdDestnAndIdVehicleOrderByArrivalDateDesc(String idDestn, String idVehicle);

}
