package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.Profile;

@Repository
public interface ProfileRepo extends JpaRepository<Profile, Long>{
	Profile findByLabel(String label);
}
