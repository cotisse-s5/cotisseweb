package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.UserView;

@Repository
public interface UserViewRepo extends JpaRepository<UserView, Long>{
	UserView findByEmailAndPwd(String email, String pwd);
	
	UserView findByIdUserAndWeightAndState(String idUser, int wieght, int state);
	
	UserView findByToken(String token);
	
	UserView[] findByLabel(String label);
	
	UserView[] findByLabelAndState(String label, int state);
}
