package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cotisse.model.Brand;

public interface BrandRepo extends JpaRepository<Brand, Long>{
	Brand findByIdBrand(String idBrand);
}
