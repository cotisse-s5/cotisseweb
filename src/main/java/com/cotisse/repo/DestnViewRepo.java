package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.DestnView;

@Repository
public interface DestnViewRepo extends JpaRepository<DestnView, Long>{
	
}
