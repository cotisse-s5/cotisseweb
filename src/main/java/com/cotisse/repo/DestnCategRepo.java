package com.cotisse.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotisse.model.DestnCateg;

@Repository
public interface DestnCategRepo  extends JpaRepository<DestnCateg, Long>{
	
	DestnCateg findByIdDestnAndIdCateg(String idDestn, String idCateg);
}
