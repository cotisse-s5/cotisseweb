package com.cotisse.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.cotisse.repo.StringSeqId;

@Entity
@Table(name="destndets")
public class DestnDets {
	@Id
	@GenericGenerator(name="destndets_seq",
			strategy = "com.cotisse.repo.StringSeqId",
			parameters = {
					@Parameter(name = StringSeqId.INCREMENT_PARAM, value = "50"),
					@Parameter(name = StringSeqId.VALUE_PREFIX_PARAMETER, value = "destndets_"),
					@Parameter(name = StringSeqId.NUMBER_FORMAT_PARAMETER, value = "%03d")
			}
	)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "destndets_seq")
	@Column(name="iddestndets")
	String idDestnDets;
	
	@Column(name="iddestn")
	String idDestn;

	@Column(name="idvehicle")
	String idVehicle;
	
	@Column(name="departuredate")
	Timestamp departureDate;
	
	@Column(name="arrivaldate")
	Timestamp arrivalDate;
	
	@Column(name="destnprice")
	double destnPrice;
	
	@Column(name="state")
	int state = 1;

	public String getIdDestnDets() {
		return idDestnDets;
	}

	public void setIdDestnDets(String idDestnDets) {
		this.idDestnDets = idDestnDets;
	}

	public String getIdDestn() {
		return idDestn;
	}

	public void setIdDestn(String idDestn) {
		this.idDestn = idDestn;
	}

	public String getIdVehicle() {
		return idVehicle;
	}

	public void setIdVehicle(String idVehicle) {
		this.idVehicle = idVehicle;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public double getDestnPrice() {
		return destnPrice;
	}

	public void setDestnPrice(double destnPrice) {
		this.destnPrice = destnPrice;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
}
