package com.cotisse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.cotisse.repo.StringSeqId;

@Entity
@Table(name="destn")
public class Destn {
	@Id
	@GenericGenerator(name="destn_seq",
			strategy = "com.cotisse.repo.StringSeqId",
			parameters = {
					@Parameter(name = StringSeqId.INCREMENT_PARAM, value = "50"),
					@Parameter(name = StringSeqId.VALUE_PREFIX_PARAMETER, value = "destn_"),
					@Parameter(name = StringSeqId.NUMBER_FORMAT_PARAMETER, value = "%03d")
			}
	)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "destn_seq")
    @Column(name="iddestn")
	String idDestn;
	
	@Column(name="iddeparture")
	String idDeparture;
	
	@Column(name="idarrival")
	String idArrival;

	public String getIdDestn() {
		return idDestn;
	}

	public void setIdDestn(String idDestn) {
		this.idDestn = idDestn;
	}

	public String getIdDeparture() {
		return idDeparture;
	}

	public void setIdDeparture(String idDeparture) {
		this.idDeparture = idDeparture;
	}

	public String getIdArrival() {
		return idArrival;
	}

	public void setIdArrival(String idarrival) {
		this.idArrival = idarrival;
	}
}
