package com.cotisse.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name="destndets_view")
public class DestnDetsView {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", updatable = false, nullable = false)
	String id;

	@Column(name="iddestndets")
	String idDestnDets;
	
	@Column(name="iddestn")
	String idDestn;

	@Column(name="idvehicle")
	String idVehicle;
	
	@Column(name="departuredate")
	Timestamp departureDate;
	
	@Column(name="arrivaldate")
	Timestamp arrivalDate;
	
	@Column(name="destnprice")
	double destnPrice;
	
	@Column(name="state")
	int state = 1;

	@Column(name="regnb")
	String regNb;
	
	@Column(name="idcateg")
	String idCateg;
	
	@Column(name="label")
	String label;

	@Column(name="nbcolumn")
	int nbColumn;
	
	@Column(name="nbrow")
	int nbRow;
	
	@Column(name="iddeparture")
	String idDeparture;
	
	@Column(name="idarrival")
	String idArrival;
	
	@Column(name="departure")
	String departure;
	
	@Column(name="arrival")
	String arrival;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdDestnDets() {
		return idDestnDets;
	}

	public void setIdDestnDets(String idDestnDets) {
		this.idDestnDets = idDestnDets;
	}

	public String getIdDestn() {
		return idDestn;
	}

	public void setIdDestn(String idDestn) {
		this.idDestn = idDestn;
	}

	public String getIdVehicle() {
		return idVehicle;
	}

	public void setIdVehicle(String idVehicle) {
		this.idVehicle = idVehicle;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public double getDestnPrice() {
		return destnPrice;
	}

	public void setDestnPrice(double destnPrice) {
		this.destnPrice = destnPrice;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getRegNb() {
		return regNb;
	}

	public void setRegNb(String regNb) {
		this.regNb = regNb;
	}

	public String getIdCateg() {
		return idCateg;
	}

	public void setIdCateg(String idCateg) {
		this.idCateg = idCateg;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getNbColumn() {
		return nbColumn;
	}

	public void setNbColumn(int nbColumn) {
		this.nbColumn = nbColumn;
	}

	public int getNbRow() {
		return nbRow;
	}

	public void setNbRow(int nbRow) {
		this.nbRow = nbRow;
	}

	public String getIdDeparture() {
		return idDeparture;
	}

	public void setIdDeparture(String idDeparture) {
		this.idDeparture = idDeparture;
	}

	public String getIdArrival() {
		return idArrival;
	}

	public void setIdArrival(String idArrival) {
		this.idArrival = idArrival;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}
}
