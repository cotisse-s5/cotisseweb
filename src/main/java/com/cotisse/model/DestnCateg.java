package com.cotisse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.cotisse.repo.StringSeqId;

@Entity
@Table(name="destncateg")
public class DestnCateg {
	@Id
	@GenericGenerator(name="destncateg_seq",
			strategy = "com.cotisse.repo.StringSeqId",
			parameters = {
					@Parameter(name = StringSeqId.INCREMENT_PARAM, value = "50"),
					@Parameter(name = StringSeqId.VALUE_PREFIX_PARAMETER, value = "destncateg_"),
					@Parameter(name = StringSeqId.NUMBER_FORMAT_PARAMETER, value = "%03d")
			}
	)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "destncateg_seq")
	@Column(name="iddestncateg")
	String idDestnCateg;

	@Column(name="iddestn")
	String idDestn;
	
	@Column(name="idcateg")
	String idCateg;
	
	@Column(name="price")
	double price;

	public String getIdDestnCateg() {
		return idDestnCateg;
	}

	public void setIdDestnCateg(String idDestnCateg) {
		this.idDestnCateg = idDestnCateg;
	}

	public String getIdDestn() {
		return idDestn;
	}

	public void setIdDestn(String idDestn) {
		this.idDestn = idDestn;
	}

	public String getIdCateg() {
		return idCateg;
	}

	public void setIdCateg(String idCateg) {
		this.idCateg = idCateg;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public DestnCateg() {}
	
	public DestnCateg(String idDestn,String idCateg, double price) {
		this.idDestn = idDestn;
		this.idCateg = idCateg;
		this.price = price;
	}
	
	public DestnCateg(String idCateg, double price) {
		this.idCateg = idCateg;
		this.price = price;
	}	
}
