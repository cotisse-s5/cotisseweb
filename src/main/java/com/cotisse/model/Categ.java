package com.cotisse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.cotisse.repo.StringSeqId;

@Entity
@Table(name="categ")
public class Categ {
	@Id
	@GenericGenerator(name="categ_seq",
			strategy = "com.cotisse.repo.StringSeqId",
			parameters = {
					@Parameter(name = StringSeqId.INCREMENT_PARAM, value = "50"),
					@Parameter(name = StringSeqId.VALUE_PREFIX_PARAMETER, value = "categ_"),
					@Parameter(name = StringSeqId.NUMBER_FORMAT_PARAMETER, value = "%03d")
			}
	)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categ_seq")
	@Column(name="idcateg")
	String idCateg;
	
	@Column(name="label")
	String label;

	@Column(name="nbcolumn")
	int nbColumn;
	
	@Column(name="nbrow")
	int nbRow;
	
	@Column(name="points")
	String points;
	
	@Column(name="description")
	String desc;
	

	public String getIdCateg() {
		return idCateg;
	}

	public void setIdCateg(String idCateg) {
		this.idCateg = idCateg;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public int getNbColumn() {
		return nbColumn;
	}

	public void setNbColumn(int nbColumn) {
		this.nbColumn = nbColumn;
	}

	public int getNbRow() {
		return nbRow;
	}

	public void setNbRow(int nbRow) {
		this.nbRow = nbRow;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public Categ() {}
}
