package com.cotisse.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.cotisse.repo.StringSeqId;

@Entity
@Table(name="users")
public class User {
	@Id
	@GenericGenerator(name="user_seq",
			strategy = "com.cotisse.repo.StringSeqId",
			parameters = {
					@Parameter(name = StringSeqId.INCREMENT_PARAM, value = "50"),
					@Parameter(name = StringSeqId.VALUE_PREFIX_PARAMETER, value = "user_"),
					@Parameter(name = StringSeqId.NUMBER_FORMAT_PARAMETER, value = "%03d")
			}
	)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @Column(name="idusers")
	String idUser;
	
	@Column(name="idprofiles")
	String idProfile;

	@Column(name="frstname")
	String frstName;
	
	@Column(name="lastname")
	String lastName;

	@Column(name="email")
	String email;
	
	@Column(name="num")
	String num;
	
	@Column(name="pwd")
	String pwd;
	
	@Column(name="token")
	String token;
	
	@Column(name="expdate")
	Timestamp expDate;
	
	@Column(name="state")	
	int state = 1;
	

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getIdProfile() {
		return idProfile;
	}

	public void setIdProfile(String idProfile) {
		this.idProfile = idProfile;
	}

	public String getFrstName() {
		return frstName;
	}

	public void setFrstName(String frstName) {
		this.frstName = frstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Timestamp getExpDate() {
		return expDate;
	}

	public void setExpDate(Timestamp expDate) {
		this.expDate = expDate;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
	
	public User() {}
	
	public User(String token) {
		this.token = token;
	}
	
	public User(UserView user) {
		this.idUser = user.getIdUser();
		this.idProfile = user.getIdProfile();
		this.frstName = user.getFrstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		this.pwd = user.getPwd();
		this.token = user.getToken();
		this.expDate = user.getExpDate();
	}
}
