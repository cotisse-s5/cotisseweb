package com.cotisse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.cotisse.repo.StringSeqId;

@Entity
@Table(name="chair")
public class Chair {
	@Id
	@GenericGenerator(name="chair_seq",
			strategy = "com.cotisse.repo.StringSeqId",
			parameters = {
					@Parameter(name = StringSeqId.INCREMENT_PARAM, value = "50"),
					@Parameter(name = StringSeqId.VALUE_PREFIX_PARAMETER, value = "chair_"),
					@Parameter(name = StringSeqId.NUMBER_FORMAT_PARAMETER, value = "%03d")
			}
	)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "chair_seq")
	@Column(name="idchair")
	String idChair;
	
	@Column(name="idvehicle")
	String idVehicle;

	@Column(name="num")
	int num;

	@Column(name="state")
	int state = 0;
	
	public String getIdChair() {
		return idChair;
	}

	public void setIdChair(String idChair) {
		this.idChair = idChair;
	}

	public String getIdVehicle() {
		return idVehicle;
	}

	public void setIdVehicle(String idVehicle) {
		this.idVehicle = idVehicle;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Chair() {}
	
	public Chair(String idVehicle) {
		this.idVehicle = idVehicle;
	}
	
	public Chair(String idVehicle, int num, int state) {
		this.idVehicle = idVehicle;
		this.num = num;
		this.state = state;
	}
}
