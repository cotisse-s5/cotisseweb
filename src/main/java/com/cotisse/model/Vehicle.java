package com.cotisse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.cotisse.repo.StringSeqId;

@Entity
@Table(name="vehicle")
public class Vehicle {
	@Id
	@GenericGenerator(name="veh_seq",
			strategy = "com.cotisse.repo.StringSeqId",
			parameters = {
					@Parameter(name = StringSeqId.INCREMENT_PARAM, value = "50"),
					@Parameter(name = StringSeqId.VALUE_PREFIX_PARAMETER, value = "veh_"),
					@Parameter(name = StringSeqId.NUMBER_FORMAT_PARAMETER, value = "%03d")
			}
	)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "veh_seq")
    @Column(name="idvehicle")
	String idVehicle;
	
	@Column(name="idbrand")
	String idBrand;

	@Column(name="idusers")
	String idUsers;
	
	@Column(name="idcateg")
	String idCateg;

	@Column(name="regnb")
	String regNb;
	
	@Column(name="nbcolumn")
	int nbColumn;
	
	@Column(name="nbrow")
	int nbRow;
	
	@Column(name="state")
	int state = 1;

	public String getIdVehicle() {
		return idVehicle;
	}

	public void setIdVehicle(String idVehicle) {
		this.idVehicle = idVehicle;
	}

	public String getIdBrand() {
		return idBrand;
	}

	public void setIdBrand(String idBrand) {
		this.idBrand = idBrand;
	}

	public String getIdUsers() {
		return idUsers;
	}

	public void setIdUsers(String idUsers) {
		this.idUsers = idUsers;
	}

	public String getIdCateg() {
		return idCateg;
	}

	public void setIdCateg(String idCateg) {
		this.idCateg = idCateg;
	}

	public String getRegNb() {
		return regNb;
	}

	public void setRegNb(String regNb) {
		this.regNb = regNb;
	}

	public int getNbColumn() {
		return nbColumn;
	}

	public void setNbColumn(int nbColumn) {
		this.nbColumn = nbColumn;
	}

	public int getNbRow() {
		return nbRow;
	}

	public void setNbRow(int nbRow) {
		this.nbRow = nbRow;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
}
