package com.cotisse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="destn_view")
public class DestnView {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", updatable = false, nullable = false)
	String id;
	
    @Column(name="iddestn")
	String idDestn;
    
    @Column(name="iddeparture")
	String idDeparture;
	
	@Column(name="idarrival")
	String idarrival;
	
	@Column(name="departure")
	String departure;
	
	@Column(name="arrival")
	String arrival;
	
	@Column(name="idcateg")
	String idCateg;
	
	@Column(name="label")
	String label;
	
	@Column(name="price")
	double price;
	
	@Transient
	public DestnCateg[] categs = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdDestn() {
		return idDestn;
	}

	public void setIdDestn(String idDestn) {
		this.idDestn = idDestn;
	}

	public String getIdDeparture() {
		return idDeparture;
	}

	public void setIdDeparture(String idDeparture) {
		this.idDeparture = idDeparture;
	}

	public String getIdarrival() {
		return idarrival;
	}

	public void setIdarrival(String idarrival) {
		this.idarrival = idarrival;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getIdCateg() {
		return idCateg;
	}

	public void setIdCateg(String idCateg) {
		this.idCateg = idCateg;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
