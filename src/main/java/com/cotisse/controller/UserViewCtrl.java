package com.cotisse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cotisse.model.UserView;
import com.cotisse.service.UserViewSrv;
import com.cotisse.tools.Resp;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserViewCtrl {
	@Autowired
	UserViewSrv userViewSrv;

	@CrossOrigin
	@PostMapping("/admin/login")
	public Resp login(@RequestBody UserView user) {
		return userViewSrv.logAsAdmin(user);
	}
	
	@CrossOrigin
	@PostMapping("/customer/login")
	public Resp loginAsCustomer(@RequestBody UserView user) {
		return userViewSrv.logAsAdmin(user);
	}

	@CrossOrigin
	@PostMapping("/checkToken")
	public Resp checkLogin(@RequestHeader String token) {
		return userViewSrv.checkToken(token);
	}

	@CrossOrigin
	@GetMapping("/driver/list")
	public Resp list(@RequestHeader(name="Authorization") String token) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? userViewSrv.listDriver() : resp;
	}
	
	@CrossOrigin
	@GetMapping("/driver/available")
	public Resp listAvailable(@RequestHeader(name="Authorization") String token) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? userViewSrv.driverAvailable() : resp;
	}
}
