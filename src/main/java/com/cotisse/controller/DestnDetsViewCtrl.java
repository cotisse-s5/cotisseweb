package com.cotisse.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.cotisse.service.DestnDetsViewSrv;
import com.cotisse.service.UserViewSrv;
import com.cotisse.tools.Resp;

@RestController
public class DestnDetsViewCtrl {
	@Autowired
	DestnDetsViewSrv destnDetsViewSrv;
	@Autowired
	UserViewSrv userViewSrv;
	
	@CrossOrigin
	@PostMapping("/destnDets/listDts")
	public Resp list(@RequestHeader(name="Authorization") String token,  @RequestBody Map<String, Object> data) {
		System.out.println("test");
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? destnDetsViewSrv.list(data) : resp;
	}
}
