package com.cotisse.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cotisse.service.UserViewSrv;
import com.cotisse.service.VehicleSrv;
import com.cotisse.service.VehicleViewSrv;
import com.cotisse.tools.Resp;

@RestController
@RequestMapping("/admin/vehicle")
public class VehicleCtrl {
	@Autowired
	VehicleSrv vehicleSrv;
	@Autowired
	VehicleViewSrv vehicleViewSrv;
	@Autowired
	UserViewSrv userViewSrv;
	
	@CrossOrigin
	@PostMapping("/insert")
	public Resp insert(@RequestHeader(name="Authorization") String token, @RequestBody Map<String, Object> data) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? vehicleSrv.insert(data) : resp;
	}
	
	@CrossOrigin
	@GetMapping("/list")
	public Resp getVehicle(@RequestHeader(name="Authorization") String token) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? vehicleViewSrv.listVehicle() : resp;
	}
	
	@CrossOrigin
	@GetMapping("/list/{idCateg}")
	public Resp getVehicleCateg(@RequestHeader(name="Authorization") String token, @PathVariable("idCateg") String idCateg) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? vehicleViewSrv.listVehilceCateg(idCateg) : resp;
	}
}
