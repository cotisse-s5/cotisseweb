package com.cotisse.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.cotisse.service.DestnDetsSrv;
import com.cotisse.service.UserViewSrv;
import com.cotisse.tools.Resp;

@RestController
public class DestnDetsCtrl {
	@Autowired
	DestnDetsSrv destnDetsSrv;
	@Autowired
	UserViewSrv userViewSrv;

	@CrossOrigin
	@PostMapping("/admin/destnDets/insert")
	public Resp insert(@RequestHeader(name="Authorization") String token, @RequestBody Map<String, Object> data) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? destnDetsSrv.insert(data) : resp;
	}
	
	@CrossOrigin
	@GetMapping("/destnDets/list")
	public Resp list(@RequestHeader(name="Authorization") String token) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? destnDetsSrv.list() : resp;
	}
}
