package com.cotisse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cotisse.service.DestnViewSrv;
import com.cotisse.service.UserViewSrv;
import com.cotisse.tools.Resp;

@RestController
@RequestMapping("/admin/destination")
public class DestViewCtrl {
	@Autowired
	UserViewSrv userViewSrv;
	@Autowired
	DestnViewSrv destnViewSrv;

	@CrossOrigin
	@GetMapping("/list")
	public Resp list(@RequestHeader(name="Authorization") String token) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? destnViewSrv.list() : resp;
	}
}
