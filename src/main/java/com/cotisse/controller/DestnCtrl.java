package com.cotisse.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cotisse.service.DestnSrv;
import com.cotisse.service.UserViewSrv;
import com.cotisse.tools.Resp;

@RestController
@RequestMapping("/admin/destn")
public class DestnCtrl {
	@Autowired
	DestnSrv destnSrv;
	@Autowired
	UserViewSrv userViewSrv;

	@CrossOrigin
	@PostMapping("/insert")
	public Resp insert(@RequestHeader(name="Authorization") String token, @RequestBody Map<String, Object> data) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? destnSrv.insert(data) : resp;
	}
}
