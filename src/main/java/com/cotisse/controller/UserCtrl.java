package com.cotisse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cotisse.model.User;
import com.cotisse.service.UserSrv;
import com.cotisse.service.UserViewSrv;
import com.cotisse.tools.Resp;

@RestController
@RequestMapping("/admin/user")
public class UserCtrl {
	@Autowired
	UserViewSrv userViewSrv;
	@Autowired
	UserSrv userSrv;

	@CrossOrigin
	@PostMapping("/insertDriver")
	public Resp insertDriver(@RequestHeader(name="Authorization") String token, @RequestBody User user) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? userSrv.insertDriver(user) : resp;
	}
}
