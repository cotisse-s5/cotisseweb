package com.cotisse.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cotisse.model.City;
import com.cotisse.service.CitySrv;
import com.cotisse.service.UserViewSrv;
import com.cotisse.tools.Resp;

@RestController
@RequestMapping("/admin/city")
public class CityCtrl {
	
	@Autowired
	CitySrv citySrv;
	@Autowired
	UserViewSrv userViewSrv;

	@CrossOrigin
	@PostMapping("/insert")
	public Resp insert(@RequestHeader(name="Authorization") String token, @RequestBody City city) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? citySrv.insert(city) : resp;
	}

	@CrossOrigin
	@GetMapping("/list")
	public Resp listCity(@RequestHeader(name="Authorization") String token) {
		token = token.split("\\s+")[1];
		Resp resp = userViewSrv.checkToken(token);
		return (resp.getData() != null)? citySrv.list() : resp;
	}
}
