package com.cotisse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.Brand;
import com.cotisse.repo.BrandRepo;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;

@Service
public class BrandSrv {
	@Autowired
	BrandRepo brandRepo;
	
	public Resp list() {
		try {
			List<Brand> brands = brandRepo.findAll();
			return new Resp(Resp.SUCCESS, brands.toArray(new Brand[brands.size()]), new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
