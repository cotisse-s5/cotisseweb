package com.cotisse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.Categ;
import com.cotisse.repo.CategRepo;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;

@Service
public class CategSrv {
	@Autowired
	CategRepo categRepo;
	
	public Resp list() {
		try {
			List<Categ> categs = categRepo.findAll();
			return new Resp(Resp.SUCCESS, categs.toArray(new Categ[categs.size()]), new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
