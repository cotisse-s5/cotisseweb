package com.cotisse.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.User;
import com.cotisse.model.UserView;
import com.cotisse.repo.UserRepo;
import com.cotisse.repo.UserViewRepo;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;
import com.cotisse.tools.Sha1;
import com.cotisse.tools.Tstp;

@Service
public class UserViewSrv {
	@Autowired
	UserViewRepo userViewRepo;
	@Autowired
	UserRepo userRepo;
	
	public Resp logAsAdmin(UserView user) {
		try {
			if(user.getEmail().isEmpty() || user.getPwd().isEmpty()) {
				List<RespMsg> err = new ArrayList<RespMsg>();
				if(user.getEmail().isEmpty())
					err.add(new RespMsg("email", "Email is empty"));
				if(user.getPwd().isEmpty())
					err.add(new RespMsg("pwd", "Password is empty"));
				return new Resp(Resp.ERR, null, err.toArray());
			}
			user = userViewRepo.findByEmailAndPwd(user.getEmail(), Sha1.encipher(user.getPwd()));
			if(user == null)
				return new Resp(Resp.ERR, new RespMsg("error", "Email or Password incorrect"));
			if(user.getWeight() != 100)
				return new Resp(Resp.ERR, new RespMsg("error", "Email does not exist"));
			this.generateExpDate(user);
			this.generateToken(user);
			this.updateToken(user);
			User tmp = new User(user.getToken());
			return new Resp(Resp.SUCCESS, tmp, new RespMsg("", "Succes"));
		} catch (Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
	
	public Resp logAsCustomer(UserView user){
		try {
			if(user.getEmail().isEmpty() || user.getPwd().isEmpty()) {
				List<RespMsg> err = new ArrayList<RespMsg>();
				if(user.getEmail().isEmpty())
					err.add(new RespMsg("email", "Email is empty"));
				if(user.getPwd().isEmpty())
					err.add(new RespMsg("pwd", "Password is empty"));
				return new Resp(Resp.ERR, null, err.toArray());
			}
			user = userViewRepo.findByEmailAndPwd(user.getEmail(), Sha1.encipher(user.getPwd()));
			if(user == null)
				return new Resp(Resp.ERR, new RespMsg("error", "Email or Password incorrect"));
			if(user.getWeight() != 1)
				return new Resp(Resp.ERR, new RespMsg("error", "Email does not exist"));
			this.generateExpDate(user);
			this.generateToken(user);
			this.updateToken(user);
			User tmp = new User(user.getToken());
			return new Resp(Resp.SUCCESS, tmp, new RespMsg("", "Succes"));
		} catch (Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
	
	public void generateExpDate(UserView admin) throws Exception {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		Timestamp expDate = Tstp.addHr(now, 1);
		admin.setExpDate(expDate);
	}
	
	public void generateToken(UserView user) throws Exception {
		String token = user.getToken() + String.valueOf(user.getExpDate().getTime());
		token = Sha1.encipher(token);
		user.setToken(token);
	}
	
	public void updateToken(UserView user) throws Exception {
		User tmp = new User(user);
		userRepo.save(tmp);
	}
	
	public Resp checkToken(String token) {
		try {
			UserView user = new UserView();
			user.setToken(token);
			if(userViewRepo.findByToken(user.getToken()) == null)
				return new Resp(Resp.ERR, null, new RespMsg("auth", "Token doesn't existe"));
			return new Resp(Resp.SUCCESS, user, new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, null, new Resp(Resp.ERR, null, new RespMsg("err", e.getMessage())));
		}
	}

	public Resp listDriver() {
		try {
			UserView[] driverList = userViewRepo.findByLabel("driver");
			if(driverList == null)
				return new Resp(Resp.ERR, new RespMsg("error","no diver"));
			return new Resp(Resp.SUCCESS, driverList, new RespMsg("", "Succes"));
		} catch (Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
	
	public Resp driverAvailable() {
		try {
			UserView[] driverList = userViewRepo.findByLabelAndState("driver", 1);
			if(driverList == null)
				return new Resp(Resp.ERR, new RespMsg("error","no diver"));
			return new Resp(Resp.SUCCESS, driverList, new RespMsg("", "Succes"));
		} catch (Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
