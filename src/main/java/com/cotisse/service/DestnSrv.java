package com.cotisse.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.Destn;
import com.cotisse.model.DestnCateg;
import com.cotisse.repo.DestnCategRepo;
import com.cotisse.repo.DestnRepo;
import com.cotisse.tools.Init;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;

@Service
public class DestnSrv {
	@Autowired
	DestnRepo destnRepo;
	@Autowired
	DestnCategRepo destnCategRepo;
	
	public Resp insert(Map<String, Object> data) {
		try {
			Destn destn = new Destn();
			Init.set(destn, data);
			try {
				System.out.println(data.get("price"));
				double cast = Double.parseDouble((String)data.get("price"));
			}catch(Exception ex) {
				return new Resp(Resp.ERR, new RespMsg("price", "Price must be a number"));
			}
			DestnCateg destnCateg = new DestnCateg();
			Init.set(destnCateg, data);
			if(destnCateg.getPrice() == 0)
				return new Resp(Resp.ERR, new RespMsg("price", "price require"));
			Destn destnExist = destnRepo.findByIdDepartureAndIdArrival( destn.getIdDeparture(), destn.getIdArrival());
			if(destnExist == null) {
				destnRepo.save(destn);
				destnExist = destnRepo.findByIdDepartureAndIdArrival(destn.getIdDeparture(), destn.getIdArrival());
				destnCateg.setIdDestn(destnExist.getIdDestn());
				destnCategRepo.save(destnCateg);
				return new Resp(Resp.SUCCESS, new RespMsg("success", "Success"));
			}else {
				if(destnCategRepo.findByIdDestnAndIdCateg(destnExist.getIdDestn(), destnCateg.getIdCateg()) != null)
					return new Resp(Resp.ERR,  new RespMsg("error", "Category already exist in this destnination"));
				destnCateg.setIdDestn(destnExist.getIdDestn());
				destnCategRepo.save(destnCateg);
				return new Resp(Resp.SUCCESS, new RespMsg("success", "Success"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
