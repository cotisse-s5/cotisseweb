package com.cotisse.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.Categ;
import com.cotisse.model.Chair;
import com.cotisse.model.User;
import com.cotisse.model.UserView;
import com.cotisse.model.Vehicle;
import com.cotisse.repo.BrandRepo;
import com.cotisse.repo.CategRepo;
import com.cotisse.repo.ChairRepo;
import com.cotisse.repo.UserRepo;
import com.cotisse.repo.UserViewRepo;
import com.cotisse.repo.VehicleRepo;
import com.cotisse.tools.Init;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;

@Service
public class VehicleSrv {
	@Autowired
	VehicleRepo vehicleRepo;
	@Autowired
	UserViewRepo userViewRepo;
	@Autowired
	UserRepo userRepo;
	@Autowired
	ChairRepo chairRepo;
	@Autowired
	CategRepo categRepo;
	@Autowired
	BrandRepo brandRepo;
	
	public Resp insert(Map<String, Object> data) {
		try {
			Vehicle vehicle = new Vehicle();
			Init.set(vehicle, data);
			for(Map.Entry<String, Object> entry : data.entrySet())
				System.out.println("key:" + entry.getKey() + " value:" + entry.getValue());
			UserView user = userViewRepo.findByIdUserAndWeightAndState(vehicle.getIdUsers(), 10, 1);
			if(user == null)
				return new Resp(Resp.ERR, new RespMsg("driver","Driver does not exist"));
			Vehicle veh = vehicleRepo.findByRegNb(vehicle.getRegNb());
			if(brandRepo.findByIdBrand(vehicle.getIdBrand()) == null)
				return new Resp(Resp.ERR, new RespMsg("brand","Brand does not exist"));
			if(veh == null) {
				Categ categ = categRepo.findByIdCateg((String)data.get("idCateg"));
				if(categ == null)
					return new Resp(Resp.ERR, new RespMsg("categ","Category does not exist"));
				vehicle.setIdCateg(categ.getIdCateg());
				vehicle.setNbColumn(categ.getNbColumn());
				vehicle.setNbRow(categ.getNbRow());
				User tmp = new User(user);
				tmp.setState(10);
				userRepo.save(tmp);
				vehicleRepo.save(vehicle);
				veh = vehicleRepo.findByRegNb(vehicle.getRegNb());
				@SuppressWarnings("unchecked")
				List<Map<String, Object>> chairList = (ArrayList<Map<String, Object>>)data.get("chairs");
				int nb = 1, state = 0;
				int chairNum = -1;
				for(int i = 0; i < chairList.size(); i++) {
					state = (Integer)chairList.get(i).get("state");
					chairNum = (state > 0)? nb++ : -1;
					Chair chair = new Chair(vehicle.getIdVehicle(), chairNum, state);
					chairRepo.save(chair);
				}
				return new Resp(Resp.SUCCESS, new RespMsg("success","success"));
			}
			return new Resp(Resp.ERR, new RespMsg("error","error"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}

	public Resp listVehicle() {
		try {
			List<Vehicle> veh = vehicleRepo.findAll();
			return new Resp(Resp.SUCCESS, veh.toArray(new Vehicle[veh.size()]), new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
