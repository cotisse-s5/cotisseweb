package com.cotisse.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.Destn;
import com.cotisse.model.DestnDets;
import com.cotisse.model.Vehicle;
import com.cotisse.repo.DestnDetsRepo;
import com.cotisse.repo.DestnRepo;
import com.cotisse.repo.VehicleRepo;
import com.cotisse.tools.Init;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;

@Service
public class DestnDetsSrv {
	@Autowired
	DestnDetsRepo destnDetsRepo;
	@Autowired
	VehicleRepo vehicleRepo;
	@Autowired
	DestnRepo destnRepo;
	
	public Resp insert(Map<String, Object> data) {
		try {
			RespMsg err = new RespMsg();
			if(Double.valueOf(data.get("destnPrice").toString()) == 0)
				err.put("price", "destination price required");
			if(((String)data.get("departureDate")).isEmpty())
				err.put("departureDate", "departure date required");
			if(((String)data.get("arrivalDate")).isEmpty())
				err.put("arrivalDate", "arrival date required");
			if(err.size() != 0)
				return new Resp(Resp.ERR, err);
			DestnDets destnDets = new DestnDets();
			Init.set(destnDets, data);
			if(destnDets.getArrivalDate().before(destnDets.getDepartureDate()))
				return new Resp(Resp.ERR, new RespMsg("arrival", "arrival date should be after departure date")); 
			Vehicle veh = vehicleRepo.findByIdVehicleAndState(destnDets.getIdVehicle(), 1);
			if(veh == null)
				return new Resp(Resp.ERR, new RespMsg("vehilce", "Vehicle doesn't exist or is not available")); 
			Destn destn = destnRepo.findByIdDestn(destnDets.getIdDestn());
			if(destn == null)
				return new Resp(Resp.ERR, new RespMsg("destination", "Destination doesn't exist"));
			DestnDets destnPre =  destnDetsRepo.findTop1ByIdDestnAndIdVehicleOrderByArrivalDateDesc(destnDets.getIdDestn(), destnDets.getIdVehicle());
			if(destnPre == null) {
				destnDetsRepo.save(destnDets);
				return new Resp(Resp.SUCCESS, new RespMsg("success", "success"));
			}
			if(destnPre.getArrivalDate().after(destnDets.getDepartureDate()) || destnPre.getArrivalDate().equals(destnDets.getDepartureDate()))
				return new Resp(Resp.ERR, new RespMsg("vehicle", "Vehicle is not available in this date"));
			destnDetsRepo.save(destnDets);
			return new Resp(Resp.SUCCESS, new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
	
	public Resp list() {
		try {
			List<DestnDets> destnDets = destnDetsRepo.findAll();
			return new Resp(Resp.SUCCESS, destnDets.toArray(new Vehicle[destnDets.size()]), new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
