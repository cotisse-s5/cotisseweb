package com.cotisse.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.DestnDetsView;
import com.cotisse.repo.DestnDetsViewRepo;
import com.cotisse.tools.Init;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;

@Service
public class DestnDetsViewSrv {
	@Autowired
	DestnDetsViewRepo destnDetsViewRepo;

	public Resp list(Map<String, Object> data) {
		try {
			RespMsg err = new RespMsg();
			if((data.get("idDeparture").toString()).isEmpty())
				err.put("departure", "departure required");
			if((data.get("idArrival").toString()).isEmpty())
				err.put("arrival", "arrival required");
			if((data.get("departureDate").toString()).isEmpty())
				err.put("departureDate", "departure date required");
			if(err.size() != 0)
				return new Resp(Resp.ERR, err);
			DestnDetsView destnDets = new DestnDetsView();
			Init.set(destnDets, data);
			DestnDetsView[] destnDetsViews = destnDetsViewRepo.findByIdDepartureAndIdArrivalAndDepartureDate(destnDets.getIdDeparture(), destnDets.getIdArrival(), destnDets.getDepartureDate());
			return new Resp(Resp.SUCCESS, destnDetsViews, new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
