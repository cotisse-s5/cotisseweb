package com.cotisse.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.Profile;
import com.cotisse.model.User;
import com.cotisse.repo.ProfileRepo;
import com.cotisse.repo.UserRepo;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;

@Service
public class UserSrv {
	@Autowired
	UserRepo userRepo;
	@Autowired
	ProfileRepo profileRepo;
	
	public Resp insertDriver(User user) {
		try {
			Profile profile = profileRepo.findByLabel("driver");
			user.setIdProfile(profile.getIdProfile());
			List<RespMsg> err = new ArrayList<RespMsg>();
			if(user.getFrstName().isEmpty())
				err.add(new RespMsg("First name", "Phone number is empty"));
			if(user.getLastName().isEmpty())
				err.add(new RespMsg("Last name", "Phone number is empty"));
			if(user.getEmail().isEmpty())
				err.add(new RespMsg("email", "Email is empty"));
			if(user.getNum().isEmpty())
				err.add(new RespMsg("num", "Phone number is empty"));
			User tmp = userRepo.findByEmail(user.getEmail());
			if(tmp != null)
				err.add(new RespMsg("email", "Email already exist"));
			tmp = userRepo.findByNum(user.getNum());
			if(tmp != null)
				err.add(new RespMsg("num", "Phone number already exist"));
			if(err.size() != 0)
				return new Resp(Resp.ERR, err.toArray());
			userRepo.save(user);
			return new Resp(Resp.SUCCESS,profile, new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
