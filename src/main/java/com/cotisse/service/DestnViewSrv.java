package com.cotisse.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.DestnCateg;
import com.cotisse.model.DestnView;
import com.cotisse.repo.DestnViewRepo;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;


@Service
public class DestnViewSrv {
	@Autowired
	DestnViewRepo destnViewRepo;
	
	public Resp list() {
		try {
			List<DestnView> destnViewList = destnViewRepo.findAll();
			if(destnViewList == null)
				return new Resp(Resp.ERR, null, new RespMsg("error", "No Destnination"));
			else {
				DestnView[] tmp = destnViewList.toArray(new DestnView[destnViewList.size()]);
				destnViewList = new ArrayList<DestnView>();
				for(int i = 0; i < tmp.length; i++) {
					if(destnViewList.size() == 0)
						destnViewList.add(tmp[i]);
					for(int j = 0; j < destnViewList.size(); j++) {
						if(tmp[i].getIdDestn().equals(destnViewList.get(j).getIdDestn()))
							break;
						if(j == destnViewList.size()-1)
							destnViewList.add(tmp[i]);
					}
				}
				DestnView[] destnView = destnViewList.toArray(new DestnView[destnViewList.size()]);
				for(int i = 0; i < destnView.length; i++) {
					List<DestnCateg> listCateg = new ArrayList<DestnCateg>();
					for(int j = 0; j < tmp.length; j++) {
						if(destnView[i].getIdDestn().equals(tmp[j].getIdDestn())) {
							listCateg.add(new DestnCateg(tmp[j].getLabel(), tmp[j].getIdCateg(), tmp[j].getPrice()));
						}
					}
					destnView[i].categs = listCateg.toArray(new DestnCateg[listCateg.size()]);
				}
				return new Resp(Resp.SUCCESS, destnView, new RespMsg("success", "Success"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
