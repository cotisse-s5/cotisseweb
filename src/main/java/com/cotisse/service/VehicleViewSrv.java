package com.cotisse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.VehicleView;
import com.cotisse.repo.VehicleViewRepo;
import com.cotisse.tools.Resp;
import com.cotisse.tools.RespMsg;

@Service
public class VehicleViewSrv {
	@Autowired
	VehicleViewRepo vehicleViewRepo;
	
	public Resp listVehicle() {
		try {
			List<VehicleView> veh = vehicleViewRepo.findAll();
			return new Resp(Resp.SUCCESS, veh.toArray(new VehicleView[veh.size()]), new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
	
	public Resp listVehilceCateg(String idCateg) {
		try {
			VehicleView[] veh = vehicleViewRepo.findByIdCateg(idCateg);
			return new Resp(Resp.SUCCESS, veh, new RespMsg("success", "success"));
		}catch(Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
