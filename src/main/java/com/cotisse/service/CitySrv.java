package com.cotisse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotisse.model.City;
import com.cotisse.repo.CityRepo;
import com.cotisse.tools.RespMsg;
import com.cotisse.tools.Resp;

@Service
public class CitySrv {
	@Autowired
	CityRepo cityRepo;
	
	public Resp insert(City city) {
		try {
			City cityExist = cityRepo.findByLabel(city.getLabel());
			if(cityExist == null) {
				cityRepo.save(city);
				return new Resp(Resp.SUCCESS, new RespMsg("success", "Success"));
			}else {
				return new Resp(Resp.ERR, new RespMsg("error", "City already exist"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
	
	public Resp list() {
		try {
			List<City> city = cityRepo.findAll();
			if(city == null)
				return new Resp(Resp.ERR, new RespMsg("error", "no City"));
			return new Resp(Resp.SUCCESS, city.toArray(new City[city.size()]), new RespMsg("success", "success"));
		} catch (Exception e) {
			e.printStackTrace();
			return new Resp(Resp.ERR, new RespMsg("error", e.getMessage()));
		}
	}
}
