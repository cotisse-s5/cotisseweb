-- -----------------------------------------------------
-- Data profiles
-- -----------------------------------------------------
insert into profiles values('profile_001','admin', 100);
insert into profiles values('profile_002','driver', 10);
insert into profiles values('profile_003','customer', 1);


-- -----------------------------------------------------
-- Data users
-- -----------------------------------------------------
insert into users values('user_001', 'profile_001', '', '', '','p11.university@gmail.com', '0341234567', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'token', '2020-01-24 23:19:26.351', 1);

insert into users values('user_002', 'profile_003', 'jean', 'Rakoto', '1990-01-24 23:19:26.351','jeanrakoto@gmail.com', '0341234567', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'token', '2020-01-24 23:19:26.351',1);
insert into users values('user_003', 'profile_003', 'Flory', 'Bousler', '1986-01-24 23:19:26.351','Flory@gmail.com', '0341234687', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'token', '2020-01-24 23:19:26.351',1);
insert into users values('user_004', 'profile_003', 'Travers', 'Tirrey', '1991-01-24 23:19:26.351','Travers@gmail.com', '0341374567', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'token', '2020-01-24 23:19:26.351',1);
insert into users values('user_005', 'profile_003', 'Webb', 'Chalmers', '2000-01-24 23:19:26.351','Webb@gmail.com', '0341234689', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'token', '2020-01-24 23:19:26.351',1);

insert into users values('user_006', 'profile_002', 'Aldric', 'Tringham', '1980-01-24 23:19:26.351','Aldric@gmail.com', '0341234567', '', '', null,1);
insert into users values('user_007', 'profile_002', 'Alejandrina', 'Morratt', '1984-01-24 23:19:26.351','Alejandrina@gmail.com', '0343234567', '', '', null,1);
insert into users values('user_008', 'profile_002', 'Steward', 'Petts', '1983-01-24 23:19:26.351','Steward@gmail.com', '0341234546', '', '', null,1);
insert into users values('user_009', 'profile_002', 'Mahala', 'Kaman', '1982-01-24 23:19:26.351','Mahala@gmail.com', '0341238767', '', '', null,1);
insert into users values('user_010', 'profile_002', 'Dorothee', 'Yukhnini', '1995-01-24 23:19:26.351','Dorothee@gmail.com', '0341564567', '', '', null,1);
insert into users values('user_011', 'profile_002', 'Fiorenze', 'Chorley', '1995-01-24 23:19:26.351','Fiorenze@gmail.com', '0336053527', '', '', null,1);
insert into users values('user_012', 'profile_002', 'Elaine', 'Jennison', '1995-01-24 23:19:26.351','Elaine@yahoo.com', '0348110642', '', '', null,1);
insert into users values('user_013', 'profile_002', 'Jaime', 'Kyteley', '1995-01-24 23:19:26.351','Jaime@gmail.com', '0326238258', '', '', null,1);
insert into users values('user_014', 'profile_002', 'Dorolice', 'Heineken', '1995-01-24 23:19:26.351','Dorolice@yahoo.com', '0333684048', '', '', null,1);
insert into users values('user_015', 'profile_002', 'Bevvy', 'Deniset', '1995-01-24 23:19:26.351','Bevvy@gmail.com', '0341845268', '', '', null,1);
insert into users values('user_016', 'profile_002', 'Luz', 'Harlick', '1995-01-24 23:19:26.351','Luz@yahoo.com', '0329502952', '', '', null,1);
insert into users values('user_017', 'profile_002', 'Adelaide', 'Loughhead', '1995-01-24 23:19:26.351','Adelaide@gmail.com', '0335640991', '', '', null,1);
insert into users values('user_018', 'profile_002', 'Denyse', 'Dermot', '1995-01-24 23:19:26.351','Denyse@yahoo.com', '0344749183', '', '', null,1);
insert into users values('user_019', 'profile_002', 'Brinna', 'Bannister', '1995-01-24 23:19:26.351','Brinna@gmail.com', '0321169543', '', '', null,1);
-- -----------------------------------------------------
-- Data driver
-- -----------------------------------------------------
--insert into driver values ('driver_1', 'Reggis', 'Wimpenny');
-- -----------------------------------------------------
-- Data city
-- -----------------------------------------------------
insert into city values('city_001', 'antananarivo');
insert into city values('city_002', 'toamasina');
insert into city values('city_003', 'mahajanga');
insert into city values('city_004', 'morondava');
insert into city values('city_005', 'fianarantsoa');
insert into city values('city_006', 'antsiranana');

-- -----------------------------------------------------
-- Data categ
-- -----------------------------------------------------
insert into categ values('categ_001', 'vip', 3, 9,'Spacious and comfortable;220V socket, Air Conditioner;Unlimited WIFI','{features:We are engaged in offering a comprehensive range of services including a VIP lounge, hot and cold beverage and breakfast}, {interior:Our bus interior is designed and developed using the best components with prescribed quality norms and safety standards}');
insert into categ values('categ_002', 'prenium', 4, 16,'Premium seats with armrest and shelf;Unlimited WIFI','{features:The seating system is highly spacious to ensure optimum comfort on long journeys. GPS tracker, WIFI hotspot and TV onboard}, {interior:Our bus interior is designed and developed using the best components with prescribed quality norms and safety standards}');
insert into categ values('categ_003', 'lite', 4, 16,'A good price-performance ratio.;Unlimited WIFI.','{features:The LITE category offers the best quality of service at any budget. All of the buses are equipped with GPS tracker, WIFI hotspot and TV onboard}, {interior:Our bus interior is designed and developed using the best components with prescribed quality norms and safety standards}');

-- -----------------------------------------------------
-- Data brand
-- -----------------------------------------------------
insert into brand values ('brand_001', 'Mercedes Sprinter 4');
insert into brand values ('brand_002', 'Mercedes Sprinter 3');

-- -----------------------------------------------------
-- Data vehicle
-- -----------------------------------------------------
insert into vehicle values ('veh_001', 'brand_001', 'user_006', 'categ_001', '', 3,4,1)
-- -----------------------------------------------------
-- registration number
-- -----------------------------------------------------
3119TBA
3479TBL
2177TBG
5475TBA

2106TBL
1148TBG
4810TBA
3347TBL
3645TBG

1443TBA
5829TBL
5020TBG
4825TBA
8988TBL
0067TBG



5328TBA
0316TBL
6349TBG
1701TBA
9404TBL